package com.techdiffuse.switchcase;

public class Main {

    public static void main(String[] args) {

        System.out.println("month = " + new SwitchDemo().getMonth(4));
        System.out.println("month = " + new SwitchDemo().getMonth(8));

        for (int dayOfWeek = 1; dayOfWeek <= 10; dayOfWeek++) {
            System.out.println("" + dayOfWeek + "' is a " + new SwitchFallthrough().getTypeOfDay(dayOfWeek));
        }

    }
}
