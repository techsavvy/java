package com.techdiffuse.swap_column;

public class Matrix {


    /**
     * for each
     *
     * @param matrix
     * @param spalte1
     * @param spalte2
     */
    void swapColumn2(int[][] matrix, int spalte1, int spalte2) {
        for (int[] zeile : matrix) {
            int temp = zeile[spalte1];
            zeile[spalte1] = zeile[spalte2];
            zeile[spalte2] = temp;
        }
        printArray(matrix);
    }

    /**
     * standard for loop
     *
     * @param matrix
     * @param spalte1
     * @param spalte2
     */
    void swapColumn1(int[][] matrix, int spalte1, int spalte2) {
        for (int i = 0; i < matrix.length; i++) {
            int temp = matrix[i][spalte1];
            matrix[i][spalte1] = matrix[i][spalte2];
            matrix[i][spalte2] = temp;
        }
        printArray(matrix);
    }

    /**
     * print matrix
     *
     * @param matrix
     */
    void printArray(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.printf("%4d  ", matrix[i][j]);
            }
            System.out.println();
        }
    }


}
