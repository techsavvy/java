package com.techdiffuse.swap_column;

public class Main {

    public static void main(String[] args) {

        int rows = 5;
        int cols = 3;
        int[][] matrix = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};

        System.out.println("rows: " + rows + "   cols: " + cols);

        // zu vertauschende Spalten
        int spalte1 = 0;
        int spalte2 = 1;
        System.out.println("Swap rows: " + spalte1 + "   and: " + spalte2);

//        System.out.println("\nGeSwapptes Array : \n");
//        new Matrix().swapColumn1(matrix, spalte1, spalte2);
//        System.out.println("\nZurueckgeswapptes Array : \n");
//        new Matrix().swapColumn1(matrix, spalte1, spalte2);

//        System.out.println("\nGeSwapptes Array : \n");
//        new Matrix().swapColumn2(matrix, spalte1, spalte2);
//        System.out.println("\nZurueckgeswapptes Array : \n");
//        new Matrix().swapColumn2(matrix, spalte1, spalte2);


    }

}
