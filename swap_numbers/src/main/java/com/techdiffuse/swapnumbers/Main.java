package com.techdiffuse.swapnumbers;

public class Main {

    public static void main(String[] args) {
        System.out.println("Swap Numbers");

        int a = 4;
        int b = 7;

//        swapNumbers1(a,b);
//        swapNumbers2(a,b);
        swapNumbers3(a,b);
    }

    static void swapNumbers3(int a, int b) {
        System.out.println("swapNumbers3 XOR");

        System.out.println("Before swap");
        System.out.println("a = " + a);
        System.out.println("b = " + b);

        a = a ^ b;
        b = a ^ b;
        a = a ^ b;

        System.out.println("After swap");
        System.out.println("a = " + a);
        System.out.println("b = " + b);
    }


    static void swapNumbers2(int a, int b) {
        System.out.println("swapNumbers2");

        System.out.println("Before swap");
        System.out.println("a = " + a);
        System.out.println("b = " + b);

        a = a * b;
        b = a / b;
        a = a / b;

        System.out.println("After swap");
        System.out.println("a = " + a);
        System.out.println("b = " + b);
    }


    static void swapNumbers1(int a, int b) {
        System.out.println("swapNumbers1");

        System.out.println("Before swap");
        System.out.println("a = " + a);
        System.out.println("b = " + b);

        a = a + b;
        b = a - b;
        a = a - b;

        System.out.println("After swap");
        System.out.println("a = " + a);
        System.out.println("b = " + b);
    }

}
