package com.techdiffuse.sumofdigits;

public class Main {

    static int remainder, sum = 0;

    public static void main(String[] args) {
        System.out.println("Sum of Digits");

//        int number = 36036; // 3 + 6 + 0 + 3 + 6 = 18
        int number = 53423; // 17
//        int number = 326430232; // 25

//        // measure time start
//        Instant start1 = Instant.now();
//
//        int sumofdigits1 = sumofdigitsIterative(number);
//        System.out.println("sumofdigits1 = " + sumofdigits1);

        int sumofdigits2 = sumofdigitsIterative2(number);
        System.out.println("sumofdigits2 = " + sumofdigits2);
//
//        // measure time end
//        Instant finish1 = Instant.now();
//        final long timeElapsed1 = Duration.between(start1, finish1).toNanos();
//        System.out.println("timeElapsed1 = " + BigDecimal.valueOf(timeElapsed1 / 1_000_000_000.0));


//        // measure time start
//        Instant start2 = Instant.now();
//
//        int sumofdigits3 = sumofdigitsRecursive(number);
//        System.out.println("sumofdigits3 = " + sumofdigits3);
//
//        // measure time end
//        Instant finish2 = Instant.now();
//        final long timeElapsed2 = Duration.between(start2, finish2).toNanos();
//        System.out.println("timeElapsed2 = " + BigDecimal.valueOf(timeElapsed2 / 1_000_000_000.0));

    }

    static int sumofdigitsRecursive(int n) {
        System.out.println("sumofdigitsRecursive n = " + n);

        if (n > 0) {
            remainder = n % 10;
            sum = sum + remainder;
            sumofdigitsRecursive(n / 10);
            System.out.println("sum = " + sum);
            return sum;
        } else {
            return 0;
        }
    }


    static int sumofdigitsIterative2(int n) {
        System.out.println("sumofdigitsIterative2");
        System.out.println("n = " + n);

        int sumofdigits = 0;

        for (sumofdigits = 0; n != 0; n = n / 10) {
            System.out.println("sumofdigits = " + sumofdigits);
            sumofdigits = sumofdigits + n % 10;
        }
        return sumofdigits;
    }


    static int sumofdigitsIterative1(int n) {
//        System.out.println("sumofdigitsIterative1");
        System.out.println("n = " + n);
//        System.out.println();

        int sumofdigits = 0;

        while (n != 0) {
            sumofdigits = sumofdigits + n % 10;
//            System.out.println("n % 10 = " + n + " % 10 = " + n % 10);
//            System.out.println("n = " + n + "  ->  (n / 10) = (" + n + " / 10) = " + n / 10);
            n = n / 10;
//            System.out.println("n after = " + n);
//            System.out.println("sumofdigits = " + sumofdigits);
//            System.out.println();
        }

        return sumofdigits;
    }

}
