package com.techdiffuse.fibonacci;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;

public class Main {

    private static long leaf = 0;

    public static void main(String[] args) {
        System.out.println("Fibonacci");

        // measure time start
        Instant start1 = Instant.now();


//        long range = 1000000000L;
        long range = 20;
        fibonacciIterative1(range);

        // measure time end
        Instant finish1 = Instant.now();
        final long timeElapsed1 = Duration.between(start1, finish1).toNanos();
        System.out.println("timeElapsed1 = " + BigDecimal.valueOf(timeElapsed1 / 1_000_000_000.0));

//        fibonacciIterative2(range);
//        int fib = fibonacciRecursive(range);
//        System.out.println(range + "\t" + fib);

        // measure time start
        Instant start2 = Instant.now();

        fibonacciRecursive(range);
//        for (int i = 0; i < range; i++) {
//            int fib = fibonacciRecursive(i);
//            System.out.println(i + "\t" + fib);
//        }

        // measure time end
        Instant finish2 = Instant.now();
        final long timeElapsed2 = Duration.between(start2, finish2).toNanos();
        System.out.println("timeElapsed2 = " + timeElapsed2 / 1_000_000_000.0);
        System.out.println("leaf = " + leaf);

    }

    static long fibonacciRecursive(long n) {
//        System.out.println("fibonacciRecursive n: " + n);
        if (n <= 1) {
//            System.out.println("n: " + n);
            leaf++;
            return n;
        }
        return fibonacciRecursive(n - 1) + fibonacciRecursive(n - 2);
    }

    static void fibonacciIterative2(long range) {
        System.out.println("fibonacciIterative2");
        long fib0 = 0;
        long fib1 = 1;

        for (long i = 0; i < range; i++) {
            System.out.println(i + "\t" + fib0);
            long fib2 = fib1 + fib0;
            fib0 = fib1;
            fib1 = fib2;
        }
    }

    static void fibonacciIterative1(long range) {
        System.out.println("fibonacciIterative1");
        long fib0 = 0;
        long fib1 = 1;

//        System.out.println("0" + "\t" + fib0);
//        System.out.println("1" + "\t" + fib1);

        long fib2 = fib1 + fib0;

        for (long i = 2; i < range; i++) {
            fib2 = fib1 + fib0;
//            System.out.println(i + "\t" + fib2);
            fib0 = fib1;
            fib1 = fib2;
        }
        System.out.println(range - 1 + "\t" + fib2);
    }
}
