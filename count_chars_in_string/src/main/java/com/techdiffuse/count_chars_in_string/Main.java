package com.techdiffuse.count_chars_in_string;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        System.out.println();
        System.out.println("Count Chars in a String");

        String str = "Our example string consists mostly of letters. However, we also added some special characters like hashtags #learntocode and punctuation marks! Also some numbers like 3, 4, 193 and 4 must not be missing.";
//        str = "Hello";
//        Map<Character, Integer> countChars = countChars(str);
        Map countChars = countChars(str);
//        System.out.println("countChars                                = " + countChars(str));

        System.out.println("countCharsSorted by value natural order   = " + sortByValue(countChars));
        System.out.println("countCharsSorted by value natural order   = " + sortByValue(countChars, Comparator.naturalOrder()));
        System.out.println("countCharsSorted by value reversed order  = " + sortByValue(countChars, Comparator.reverseOrder()));

        System.out.println();

        System.out.println("countCharsSorted by key natural order   = " + sortByKey(countChars));
        System.out.println("countCharsSorted by key natural order   = " + sortByKey(countChars, Comparator.naturalOrder()));
        System.out.println("countCharsSorted by key reversed order  = " + sortByKey(countChars, Comparator.reverseOrder()));
    }

    static boolean isSpecialChar(String s) {
        return (s == null) ? false : s.matches("[^A-Za-z0-9]"); // without whitespaces
//        return (s == null) ? false : s.matches("[^A-Za-z0-9 ]"); // including whitespaces
    }

    static Map<Character, Integer> countChars(String s) {
        Map<Character, Integer> countChars = new HashMap<>();
        int specialChars = 0;

        for (char ch : s.toCharArray()) {

            if (isSpecialChar(String.valueOf(ch))) {
//                System.out.println("isSpecialChar(String.valueOf(ch)) = " + isSpecialChar(String.valueOf(ch)));
                System.out.println("ch = " + ch);
                specialChars++;
            }

            if (!countChars.containsKey(ch)) {
                countChars.put(Character.toLowerCase(ch), 1);
//                countChars.put(ch, 1);
            } else {
//                System.out.println("countChars.get(ch) = " + countChars.get(ch));
                countChars.put(Character.toLowerCase(ch), countChars.get(ch) + 1);
//                countChars.put(ch, countChars.get(ch) + 1);
            }
//            System.out.println("ch = " + ch);
//            System.out.println("countChars.get(ch) = " + countChars.get(ch));
//            System.out.println("countChars.containsKey(ch) = " + countChars.containsKey(ch));
//            System.out.println("countChars = " + countChars);
//            System.out.println();
        }
        System.out.println("specialChars = " + specialChars);
        return countChars;
    }

    /**
     * Natural Order is the default order if called without comparator
     *
     * @param countChars
     * @param <T>
     * @return
     */
    static <T> Map<Character, Integer> sortByValue(Map<Character, Integer> countChars) {
        return sortByValue(countChars, Comparator.naturalOrder());
    }

    static <T> Map<Character, Integer> sortByValue(Map<Character, Integer> countChars, Comparator<? super T> c) {
        Map<Character, Integer> result = countChars
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue((Comparator<? super Integer>) c))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        return result;
    }


    /**
     * Natural Order is the default order if called without comparator
     *
     * @param countChars
     * @param <T>
     * @return
     */
    static <T> Map<Character, Integer> sortByKey(Map<Character, Integer> countChars) {
        return sortByKey(countChars, Comparator.naturalOrder());
    }

    static <T> Map<Character, Integer> sortByKey(Map<Character, Integer> countChars, Comparator<? super T> c) {
        Map<Character, Integer> result = countChars
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey((Comparator<? super Character>) c))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        return result;
    }


}

