package com.techdiffuse.christmas_tree;

public class ChristmasTree1 {

    void generate(int height) {
        System.out.println();

        int widthCounter = 1;
        int maxWidth = (height * 2) - 1;
        int center = maxWidth / 2 + 1;


        // draw tree
        for (int i = 1; i <= height; i++) {

            int indent = height - i;

            // print indent
            for (int j = 1; j <= indent; j++) {
                System.out.print(" ");
            }

            // print stars
            for (int j = 1; j <= widthCounter; j++) {
                System.out.print("*");
            }

            // new row of stars
            System.out.println();

            // increase width for next row
            widthCounter = widthCounter + 2;
        }

        // lower end of the tree
        for (int i = 0; i < (widthCounter - 2); i++) {
            System.out.print("-");
        }
        System.out.println();


        // trunk of the tree
        for (int i = 0; i < 2 ; i++) { // height
            for (int j = 1; j <= center ; j++) {
                if(j == center){
                    System.out.println("\"");
                } else {
                    System.out.print(" ");
                }

            }

        }

    }
}
