package com.techdiffuse.anagram;

import java.util.Arrays;

public class Anagram {

    /**
     * first anagram solution
     *
     * @param s1
     * @param s2
     * @return
     */
    boolean isAnagram1(String s1, String s2) {

        // if the length is not equal they can't be anagrams
        if (s1.length() != s2.length()) {
            return false;
        }

        // convert strings to char arrays
        char s1Array[] = s1.toCharArray();
        char s2Array[] = s2.toCharArray();

        // sort arrays
        Arrays.sort(s1Array);
        Arrays.sort(s2Array);

        // make a debugging output
        printArraysSorted(s1Array, s2Array);

        // check if arrays are equal
        for (int i = 0; i < s1.length(); i++) {
            if (s1Array[i] != s2Array[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * print char arrays in sorted order
     *
     * @param s1Array
     * @param s2Array
     */
    private void printArraysSorted(char[] s1Array, char[] s2Array) {
        for (char c : s1Array) {
            System.out.print(c + " ");
        }
        System.out.println();

        for (char c : s2Array) {
            System.out.print(c + " ");
        }
        System.out.println();
    }

}