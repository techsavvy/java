package com.techdiffuse.file_read_write;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Copy and Rotate
 */
public class Lena {

    /**
     * Create Copy
     *
     * @param path
     * @throws FileNotFoundException
     */
    public void createCopy(String path) throws FileNotFoundException {

        Scanner sc1 = new Scanner(new File(path));
        Formatter f1 = new Formatter("data/lenaCopy.pgm");

        // create copy
        while (sc1.hasNext()) {
            f1.format(sc1.nextLine() + "\n");
        }
        f1.close();
        sc1.close();
    }

    // hat nicht geklappt, der name vertical ist zudem falsch
//    /**
//     * vertical
//     *
//     * @param path
//     * @throws FileNotFoundException
//     */
//    public void vertical(String path) throws FileNotFoundException {
//
//        Scanner sc2 = new Scanner(new File(path));
//        Formatter f2 = new Formatter("data/lenaVertical.pgm");
//
//        String header = "";
//        String comment = "";
//        int breite = 0;
//        int hoehe = 0;
//        int maxGrau = 0;
//        int[][] lena;
//        int[][] lenaGedreht;
//
//        header = sc2.nextLine();
//        comment = sc2.nextLine();
//        breite = sc2.nextInt();
//        hoehe = sc2.nextInt();
//        maxGrau = sc2.nextInt();
//
//        lena = readFromFile(sc2, hoehe, breite);
//
//        // rotate
//        int downCountHeight = breite - 1;
//        lenaGedreht = new int[hoehe][breite];
//        for (int i = 0; i < hoehe; i++) {
//            for (int j = 0; j < breite; j++) {
//                // i,j                      = von links oben nach rechts unten
//                // downCountHeight,j        = von links unten nach rechts oben
//                lenaGedreht[j][i] = lena[i][j];
//            }
//            downCountHeight--;
//        }
//        writeToFile(f2, lenaGedreht, header, comment, breite, hoehe, maxGrau);
//    }

    // macht nicht das, was ich erhofft habe. Es rotiert um 90° und spiegelt gleichzeitig
//    /**
//     * mirror at XY Axis (diagonal)
//     *
//     * @param path
//     * @throws FileNotFoundException
//     */
//    public void mirrorAtXYAxis(String path) throws FileNotFoundException {
//
//        Scanner sc2 = new Scanner(new File(path));
//        Formatter f2 = new Formatter("data/lenaMirrorAtXYAxis.pgm");
//
//        String header = "";
//        String comment = "";
//        int breite = 0;
//        int hoehe = 0;
//        int maxGrau = 0;
//        int[][] lena;
//        int[][] lenaGedreht;
//
//        header = sc2.nextLine();
//        comment = sc2.nextLine();
//        breite = sc2.nextInt();
//        hoehe = sc2.nextInt();
//        maxGrau = sc2.nextInt();
//
//        lena = readFromFile(sc2, hoehe, breite);
//
//        // rotate
//        lenaGedreht = new int[hoehe][breite];
//        for (int i = 0; i < hoehe; i++) {
//            for (int j = 0; j < breite; j++) {
//                lenaGedreht[j][i] = lena[i][j];
//            }
//        }
//        writeToFile(f2, lenaGedreht, header, comment, breite, hoehe, maxGrau);
//    }

    /**
     * mirror at X Axis (not the same as 180 Degree Rotation)
     *
     * @param path
     * @throws FileNotFoundException
     */
    public void mirrorAtXAxis(String path) throws FileNotFoundException {

        Scanner sc2 = new Scanner(new File(path));
        Formatter f2 = new Formatter("data/lenaMirrorAtXAxis.pgm");

        String header = "";
        String comment = "";
        int breite = 0;
        int hoehe = 0;
        int maxGrau = 0;
        int[][] lena;
        int[][] lenaGedreht;

        header = sc2.nextLine();
        comment = sc2.nextLine();
        breite = sc2.nextInt();
        hoehe = sc2.nextInt();
        maxGrau = sc2.nextInt();

        lena = readFromFile(sc2, hoehe, breite);

        // rotate
        int downCountHeight = hoehe - 1;
        lenaGedreht = new int[hoehe][breite];
        for (int i = 0; i < hoehe; i++) {
            for (int j = 0; j < breite; j++) {
                // i,j                      = von links oben nach rechts unten
                // downCountHeight,j        = von links unten nach rechts oben
                lenaGedreht[downCountHeight][j] = lena[i][j];
            }
            downCountHeight--;
        }
        writeToFile(f2, lenaGedreht, header, comment, breite, hoehe, maxGrau);
    }

    /**
     * mirror at Y Axis
     *
     * @param path
     * @throws FileNotFoundException
     */
    public void mirrorAtYAxis(String path) throws FileNotFoundException {

        Scanner sc2 = new Scanner(new File(path));
        Formatter f2 = new Formatter("data/lenaMirrorAtYAxis.pgm");

        String header = "";
        String comment = "";
        int breite = 0;
        int hoehe = 0;
        int maxGrau = 0;
        int[][] lena;
        int[][] lenaGedreht;

        header = sc2.nextLine();
        comment = sc2.nextLine();
        breite = sc2.nextInt();
        hoehe = sc2.nextInt();
        maxGrau = sc2.nextInt();

        lena = readFromFile(sc2, hoehe, breite);

        // rotate
        lenaGedreht = new int[hoehe][breite];
        for (int i = 0; i < hoehe; i++) {

            int downCountWidth = breite - 1;
            for (int j = 0; j < breite; j++) {
                // i,j                  = von links oben nach rechts unten
                // i,downCountWidth     = von rechts oben nach links unten
                lenaGedreht[i][downCountWidth] = lena[i][j];
                downCountWidth--;
            }
        }
        writeToFile(f2, lenaGedreht, header, comment, breite, hoehe, maxGrau);
    }

    /**
     * Rotate 270 Degree
     *
     * @param path
     * @throws FileNotFoundException
     */
    public void rotate270(String path) throws FileNotFoundException {

        Scanner sc2 = new Scanner(new File(path));
        Formatter f2 = new Formatter("data/lenaRotate270.pgm");

        String header = "";
        String comment = "";
        int breite = 0;
        int hoehe = 0;
        int maxGrau = 0;
        int[][] lena;
        int[][] lenaGedreht;

        header = sc2.nextLine();
        comment = sc2.nextLine();
        breite = sc2.nextInt();
        hoehe = sc2.nextInt();
        maxGrau = sc2.nextInt();

        lena = readFromFile(sc2, hoehe, breite);

        // rotate
        lenaGedreht = new int[hoehe][breite];
        for (int i = 0; i < hoehe; i++) {

            int downCountHeight = hoehe - 1;
            for (int j = 0; j < breite; j++) {
                // i,j                  = von links oben nach rechts unten
                // downCountHeight,i    = von links unten nach rechts oben
                lenaGedreht[downCountHeight][i] = lena[i][j];
                downCountHeight--;
            }
        }
//        new MatrixPrinter().create(lenaGedreht);

        writeToFile(f2, lenaGedreht, header, comment, breite, hoehe, maxGrau);
    }

    /**
     * Rotate 180 Degree
     *
     * @param path
     * @throws FileNotFoundException
     */
    public void rotate180(String path) throws FileNotFoundException {

        Scanner sc2 = new Scanner(new File(path));
        Formatter f2 = new Formatter("data/lenaRotate180.pgm");

        String header = "";
        String comment = "";
        int breite = 0;
        int hoehe = 0;
        int maxGrau = 0;
        int[][] lena;
        int[][] lenaGedreht;

        header = sc2.nextLine();
        comment = sc2.nextLine();
        breite = sc2.nextInt();
        hoehe = sc2.nextInt();
        maxGrau = sc2.nextInt();

        lena = readFromFile(sc2, hoehe, breite);

        // rotate
        int downCountHeight = hoehe - 1;

        lenaGedreht = new int[hoehe][breite];
        for (int i = 0; i < hoehe; i++) {

            int downCountWidth = breite - 1; // bei jedem neuem Lauf zurücksetzen
            for (int j = 0; j < breite; j++) {
                // i,j                              = von links oben nach rechts unten
                // downCountHeight,downCountWidth   = von rechts unten nach links oben
                lenaGedreht[downCountHeight][downCountWidth] = lena[i][j];
                downCountWidth--;
            }
            downCountHeight--;
        }
//        new MatrixPrinter().create(lenaGedreht);

        writeToFile(f2, lenaGedreht, header, comment, breite, hoehe, maxGrau);
    }


    /**
     * Rotate 90 Degree
     *
     * @param path
     * @throws FileNotFoundException
     */
    public void rotate90(String path) throws FileNotFoundException {

        Scanner sc2 = new Scanner(new File(path));
        Formatter f2 = new Formatter("data/lenaRotate90.pgm");

        String header = "";
        String comment = "";
        int breite = 0;
        int hoehe = 0;
        int maxGrau = 0;
        int[][] lena;
        int[][] lenaGedreht;

        header = sc2.nextLine();
        comment = sc2.nextLine();
        breite = sc2.nextInt();
        hoehe = sc2.nextInt();
        maxGrau = sc2.nextInt();

        lena = readFromFile(sc2, hoehe, breite);

        // rotate
        int downCount = breite - 1;
        lenaGedreht = new int[hoehe][breite];
        for (int i = 0; i < hoehe; i++) {
            for (int j = 0; j < breite; j++) {
                // i,j          = von links oben nach rechts unten
                // j,downCount  = von rechts oben nach links unten
                lenaGedreht[j][downCount] = lena[i][j];
            }
            downCount--;
        }
//        new MatrixPrinter().create(lenaGedreht);

        writeToFile(f2, lenaGedreht, header, comment, breite, hoehe, maxGrau);
    }

    /**
     * Read data from File
     *
     * @param sc2
     * @param hoehe
     * @param breite
     * @return
     */
    private int[][] readFromFile(Scanner sc2, int hoehe, int breite) {

        int[][] matrix = new int[hoehe][breite];
        for (int i = 0; i < hoehe; i++) {
            for (int j = 0; j < breite; j++) {
                int temp = sc2.nextInt();
//                System.out.print("temp: " + temp + " ");
                matrix[i][j] = temp;
            }
//            System.out.println();
        }
        return matrix;
    }

    /**
     * Write Data to File
     *
     * @param f2
     * @param matrix
     * @param header
     * @param comment
     * @param breite
     * @param hoehe
     * @param maxGrau
     */
    private void writeToFile(Formatter f2, int[][] matrix, String header, String comment, int breite, int hoehe, int maxGrau) {
        f2.format(header + "\n");
        f2.format(comment + "\n");
        f2.format(breite + " " + hoehe + "\n");
        f2.format(maxGrau + "\n");

        // write to file
        int counter = 1;
        for (int[] zeile : matrix) {
            for (int spalte : zeile) {
                // No line should be longer than 70 characters.
                if (counter % 70 == 0) {
                    f2.format("\n");
                }
                f2.format(spalte + " ");
                counter++;
            }
//            f2.format("\n");
        }
        f2.close();
    }
}
