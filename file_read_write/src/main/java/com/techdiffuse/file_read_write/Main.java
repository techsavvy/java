package com.techdiffuse.file_read_write;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        // path and file
        String folder = "data/";
        String file1 = "matrix1.dat";
        String file2 = "matrix1Copy.dat";
        String file3 = "matrix1Rotate.dat";
        String lena = "lena.pgm";

        String[][] matrix = {{"0_0", "0_1", "0_2"}, {"1_0", "1_1", "1_2"}, {"2_0", "2_1", "2_2"}};

        new MatrixCreator().create(folder + file1, matrix);
        new ReaderWriter().create(folder + file1, folder + file2);
        new MatrixRotater().create(folder + file3, matrix);

        new Lena().createCopy(folder + lena);
        new Lena().rotate90(folder + lena);
        new Lena().rotate180(folder + lena);
        new Lena().rotate270(folder + lena);
        new Lena().mirrorAtYAxis(folder + lena);
        new Lena().mirrorAtXAxis(folder + lena); // not the same as 180 degree rotation
//        new Lena().mirrorAtXYAxis(folder + lena); // fehlerhaft
//        new Lena().vertical(folder + lena); // fehlerhaft

//        new BufferReader().create(folder + lena);
    }
}
