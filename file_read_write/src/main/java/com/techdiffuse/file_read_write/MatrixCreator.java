package com.techdiffuse.file_read_write;

import java.io.FileNotFoundException;
import java.util.Formatter;

public class MatrixCreator {

    public void create(String path, String[][] matrix) throws FileNotFoundException {

        Formatter f = new Formatter(path);

        // height
        for (String[] row : matrix) {
            // width
            for (String column : row) {
                f.format(column + "   ");
            }
            f.format("\n");
        }
        f.close();

        new MatrixPrinter().create(matrix);
    }
}
