package com.techdiffuse.file_read_write;

import java.io.*;

/**
 * Buffered Reader und Print Writer
 */
public class BufferReader {

    public void create(String path) throws IOException {

        BufferedReader puffer1 = new BufferedReader(new FileReader(new File(path)));
        String zeile1;

        while ((zeile1 = puffer1.readLine()) != null) {
            System.out.println(zeile1);
        }
        puffer1.close();


        // Print Writer Test
        BufferedReader puffer2 = new BufferedReader(new FileReader(new File(path)));
        PrintWriter textausgabe = new PrintWriter(System.out);
        String zeile2;

        while ((zeile2 = puffer2.readLine()) != null) {
            textausgabe.println(zeile2);
        }
        puffer2.close();

        textausgabe.flush();
        textausgabe.close();
    }
}
