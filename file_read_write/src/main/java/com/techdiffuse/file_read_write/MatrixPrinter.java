package com.techdiffuse.file_read_write;

public class MatrixPrinter {

    public void create(String[][] matrix) {

        System.out.println();

        // height
        for (String[] row : matrix) {
            // width
            for (String column : row) {
                System.out.print(column + "   "); // for console output
            }
            System.out.println(); // for console output
        }
    }

    public void create(int[][] matrix) {

        System.out.println();

        // height
        for (int[] row : matrix) {
            // width
            for (int column : row) {
                System.out.print(column + "   "); // for console output
            }
            System.out.println(); // for console output
        }
    }
}
