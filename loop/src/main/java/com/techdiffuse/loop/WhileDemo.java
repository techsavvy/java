package com.techdiffuse.loop;

class WhileDemo {

    /**
     * count while temp is smaller than count
     */
    void doWhile(int count) {
        int temp = 1;
        while (temp < count) {
            System.out.println("Count is: " + temp);
            temp++;
        }
    }
}

